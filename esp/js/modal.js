$(document).ready(function() {

  // Pega o Vídeo do botão usando o embed do Youtube
  
  var $videoSrc;  
  $('.video-btn').click(function() {
      $videoSrc = $(this).data( "src" );
  });
  console.log($videoSrc);
  
    
    
  // Quando abrir o Modal dá autoplay
  $('#myModal').on('shown.bs.modal', function (e) {
      
  // Inicia o video sem mostrar o "videos relacionados"
  $("#video").attr('src',$videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" ); 
  })
    

  $('#myModal').on('hide.bs.modal', function (e) {
      
      $("#video").attr('src',$videoSrc); 
  }) 
      
});